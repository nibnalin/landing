Hello! Welcome to the coala.io landing page of the future! This is a WIP, the [coala/website](https://github.com/coala/website) repo is the current one.


![](screenshots/header.png)

## Run it in Docker

Clone the repository, then:

```
$ docker build -t coala/landing .
$ docker run -e "GITHUB_API_KEY=<your key>" -p 8000:8000 coala/landing
```

For debugging, use the `-t -i` arguments to see the output of the container.

## Get Started


```
$ git clone git@gitlab.com:coala/landing.git
$ cd landing
```

#### STEP 1
```
$ cd backend
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
```

#### STEP 2

Add GitHub API key in backend/coala_web/settings.py file

```
GITHUB_API_KEY = ''
```

#### STEP 3
After this, we have to start cron jobs, cron settings are in
`backend/coala_web/settings.py` file, in the variable CRONJOBS:

```
CRONJOBS = [
  ('*/200 * * * *', 'bears.cron.fetch_bears'),
  ('*/200 * * * *', 'org.cron.fetch_contributors')
]
```

The time of jobs can be modified accordingly. [Reference for time codes](http://crontab.guru)

#### STEP 4
Then start the crons with

```
$ python manage.py crontab add
```

#### STEP 5
Now we're ready to start Django server
```
$ python manage.py runserver
```

Hooray! The site is up and running at http://localhost:8000

-----

### Basic Structure

HOME TAB

![](screenshots/coala.png)


MEET THE COMMUNITY SECTION

![](screenshots/about.png)

LANGUAGES TAB

![](screenshots/language.png)



GET INVOLVED TAB

![](screenshots/getinvolved.png)



COALA ONLINE TAB

![](screenshots/language.png)

-----
## Contribution Guidelines
[Refer Here](https://github.com/coala/coala/blob/master/README.rst)


-----
## License
![](https://img.shields.io/github/license/coala/coala.svg)
